import argparse
from typing import TextIO
from enum import Enum, auto


class BingoCell:
    def __init__(self, number):
        self._number = int(number)
        self._called = False

    def call(self, number):
        if number == self._number:
            self._called = True

    def get_number(self):
        return self._number

    def is_called(self):
        return self._called


class BingoBoard:
    def __init__(self, text):
        self.board = []
        for row_text in text:
            row = []
            for number in row_text.split():
                row.append(BingoCell(number))
            self.board.append(row)

    def call_number(self, number: int):
        for row in self.board:
            for cell in row:
                cell.call(number)

    def is_win(self):
        # Check rows
        for row in self.board:
            win = True
            for cell in row:
                if not cell.is_called():
                    win = False
            if win:
                return True

        # Check columns
        num_columns = len(self.board[0])
        num_rows = len(self.board)
        for column_index in range(num_columns):
            win = True
            for row_index in range(num_rows):
                cell = self.board[row_index][column_index]
                if not cell.is_called():
                    win = False
            if win:
                return True

        return False

    def get_score(self):
        score = 0
        for row in self.board:
            for cell in row:
                if not cell.is_called():
                    score += cell.get_number()
        return score


class PuzzleState(Enum):
    NUMBERS = auto()
    BOARDS = auto()


def part_1(input_file: TextIO):
    input_text = input_file.read()

    state = PuzzleState.NUMBERS
    numbers = []
    boards = []
    board_text = []

    for line in input_text.split('\n'):
        if state == PuzzleState.NUMBERS:
            if len(line) == 0:
                state = PuzzleState.BOARDS
            else:
                numbers = [int(x) for x in line.split(',')]
        elif state == PuzzleState.BOARDS:
            if len(line) == 0:
                boards.append(BingoBoard(board_text))
                board_text = []
            else:
                board_text.append(line)
    boards.append(BingoBoard(board_text))

    for number in numbers:
        for board in boards:
            board.call_number(number)
            if board.is_win():
                return board.get_score() * number

    return 0


def part_2(input_file: TextIO):
    input_text = input_file.read()

    state = PuzzleState.NUMBERS
    numbers = []
    boards = []
    board_text = []

    for line in input_text.split('\n'):
        if state == PuzzleState.NUMBERS:
            if len(line) == 0:
                state = PuzzleState.BOARDS
            else:
                numbers = [int(x) for x in line.split(',')]
        elif state == PuzzleState.BOARDS:
            if len(line) == 0:
                boards.append(BingoBoard(board_text))
                board_text = []
            else:
                board_text.append(line)
    boards.append(BingoBoard(board_text))

    boards_won = []
    last_score = 0

    for number in numbers:
        if len(boards_won) == len(boards):
            break
        for board_index, board in enumerate(boards):
            if board_index in boards_won:
                continue
            board.call_number(number)
            if board.is_win():
                last_score = board.get_score() * number
                boards_won.append(board_index)

    return last_score


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 4')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
